#define SOUT 8
#define S0 4
#define S1 5
#define S2 6
#define S3 7

const byte RED = 3;
const byte GREEN = 10;
const byte BLUE = 11;
const byte BTN = 2;
const byte LPIN = 13;

// Stores frequency read by the photodiodes
int redFrequency = 0;
int greenFrequency = 0;
int blueFrequency = 0;

// Stores the red. green and blue colors
int redColor = 0;
int greenColor = 0;
int blueColor = 0;

int buttonState = 0;

void setup()
{
  // Setting the outputs
  pinMode(S0, OUTPUT);
  pinMode(S1, OUTPUT);
  pinMode(S2, OUTPUT);
  pinMode(S3, OUTPUT);

  // Setting the sensorOut as an input
  pinMode(SOUT, INPUT);

  // Setting frequency scaling to 20%
  digitalWrite(S0, HIGH);
  digitalWrite(S1, LOW);

  // Begins serial communication
  Serial.begin(9600);
  blueTooth.begin(9600);
  
  pinMode(LPIN, OUTPUT);
  pinMode(BTN, INPUT);
}

void loop()
{
  buttonState = digitalRead(BTN);

  if (buttonState == HIGH)
  {
    digitalWrite(LPIN, HIGH);

    // Setting RED (R) filtered photodiodes to be read
    digitalWrite(S2, LOW);
    digitalWrite(S3, LOW);

    redFrequency = pulseIn(SOUT, LOW);
    long redColor = map(redFrequency, 16000, 3012, 0, 255);

    Serial.print(" FR: ");
    Serial.print(redFrequency);
    Serial.print(" ");
    Serial.print(" Red = ");
    Serial.print(redColor);

    delay(100);

    //    Setting GREEN (G) filtered photodiodes to be read
    digitalWrite(S2, HIGH);
    digitalWrite(S3, HIGH);

    greenFrequency = pulseIn(SOUT, LOW);
    long greenColor = map(greenFrequency, 12000, 5600, 0, 255);

    Serial.print(" FG: ");
    Serial.print(greenFrequency);
    Serial.print(" ");
    Serial.print(" Green = ");
    Serial.print(greenColor);
    delay(100);

    // Setting BLUE (B) filtered photodiodes to be read
    digitalWrite(S2, LOW);
    digitalWrite(S3, HIGH);

    blueFrequency = pulseIn(SOUT, LOW);
    long blueColor = map(blueFrequency, 2000, 2550, 0, 255);

    Serial.print(" FB: ");
    Serial.print(blueFrequency);
    Serial.print(" ");
    Serial.print(" Blue = ");
    Serial.print(blueColor);
    delay(500);

    if (redColor > greenColor && redColor > blueColor)
    {
      Serial.println(" - RED detected!");
      setRGBColor(255u, 0, 0);
    }

    if (greenColor > redColor && greenColor > blueColor)
    {
      Serial.println(" - GREEN detected!");
      setRGBColor(0, 255u, 0);
    }

    if (blueColor > redColor && blueColor > greenColor)
    {
      Serial.println(" - BLUE detected!");
      setRGBColor(0, 0, 255u);
    }
  }
  else
  {
    // turn LED off:
    digitalWrite(LPIN, LOW);
    setRGBColor(0, 0, 0);
  }
}

void setRGBColor(uint8_t red, uint8_t green, uint8_t blue)
{
  analogWrite(RED, red);
  analogWrite(GREEN, green);
  analogWrite(BLUE, blue);
}

