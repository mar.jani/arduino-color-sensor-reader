#define SOUT 8
#define S0 4
#define S1 5
#define S2 6
#define S3 7
const byte RED = 3;
const byte GREEN = 10;
const byte BLUE = 11;
const byte BTN = 2;
const byte LPIN = 13;
// Stores frequency read by the photodiodes
int redFrequency = 0;
int greenFrequency = 0;
int blueFrequency = 0;
// Stores the red. green and blue colors
int redColor = 0;
int greenColor = 0;
int blueColor = 0;
int buttonState = 0;
bool beforTransferRGB = true;
void setup()
{
  // Setting the outputs
  pinMode(S0, OUTPUT);
  pinMode(S1, OUTPUT);
  pinMode(S2, OUTPUT);
  pinMode(S3, OUTPUT);
  // Setting the sensorOut as an input
  pinMode(SOUT, INPUT);
  // Setting frequency scaling to 20%
  digitalWrite(S0, HIGH);
  digitalWrite(S1, LOW);
  // Begins serial communication
  Serial.begin(9600);
  pinMode(LPIN, OUTPUT);
  pinMode(BTN, INPUT);
}
void loop()
{
  buttonState = digitalRead(BTN);
  if (buttonState == HIGH)
  {
    digitalWrite(LPIN, HIGH);
    // Setting RED (R) filtered photodiodes to be read
    digitalWrite(S2, LOW);
    digitalWrite(S3, LOW);
    redFrequency = pulseIn(SOUT, LOW);
    int redColor = map(redFrequency, 16000, 3012, 0, 255);
    // Setting GREEN (G) filtered photodiodes to be read
    digitalWrite(S2, HIGH);
    digitalWrite(S3, HIGH);
    greenFrequency = pulseIn(SOUT, LOW);
    int greenColor = map(greenFrequency, 12000, 5600, 0, 255);
    // Setting BLUE (B) filtered photodiodes to be read
    digitalWrite(S2, LOW);
    digitalWrite(S3, HIGH);
    blueFrequency = pulseIn(SOUT, LOW);
    int blueColor = map(blueFrequency, 2000, 2550, 0, 255);
    if (beforTransferRGB == true) {


      Serial.print(redColor);
      Serial.print(",");
      Serial.print(greenColor);
      Serial.print(",");
      Serial.print(blueColor);
      Serial.println("");
      beforTransferRGB = false;
    }
    

    if (redColor > greenColor && redColor > blueColor)
    {
      setRGBColor(255u, 0, 0);
    }
    if (greenColor > redColor && greenColor > blueColor)
    {
      setRGBColor(0, 255u, 0);
    }
    if (blueColor > redColor && blueColor > greenColor)
    {
      setRGBColor(0, 0, 255u);
    }
  }
  else
  {
    // turn LED off:
    digitalWrite(LPIN, LOW);
    setRGBColor(0, 0, 0);
    beforTransferRGB = true;
  }
}
void setRGBColor(uint8_t red, uint8_t green, uint8_t blue)
{
  analogWrite(RED, red);
  analogWrite(GREEN, green);
  analogWrite(BLUE, blue);
}
