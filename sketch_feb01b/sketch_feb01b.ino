#define NO_SAMPLES 100u  
 #define THRESHOLD  5u  
 // Color Thresholds  
 #define IDLE_RED_THRESHOLD   287u  
 #define IDLE_GREEN_THRESHOLD  95u  
 #define IDLE_BLUE_THRESHOLD   303u  
 #define RED_RED_THRESHOLD    164u  
 #define RED_GREEN_THRESHOLD   72u  
 #define RED_BLUE_THRESHOLD   174u  
 #define GREEN_RED_THRESHOLD   207u  
 #define GREEN_GREEN_THRESHOLD  65u  
 #define GREEN_BLUE_THRESHOLD  219u  
 #define BLUE_RED_THRESHOLD   227u  
 #define BLUE_GREEN_THRESHOLD  65u  
 #define BLUE_BLUE_THRESHOLD   241u  
 #define L_GRN_RED_THRESHOLD   157u  
 #define L_GRN_GREEN_THRESHOLD  51u  
 #define L_GRN_BLUE_THRESHOLD  166u  
 #define YLOW_RED_THRESHOLD   107u  
 #define YLOW_GREEN_THRESHOLD  46u  
 #define YLOW_BLUE_THRESHOLD   114u  
 #define ORNG_RED_THRESHOLD   121u  
 #define ORNG_GREEN_THRESHOLD  52u  
 #define ORNG_BLUE_THRESHOLD   128u  
 // Sensor Connections  
 const byte S0  = 8;  
 const byte S1  = 9;  
 const byte S2  = 10;  
 const byte S3  = 11;  
 const byte Sout = 12;  
 const byte RED  = 3;  
 const byte GREEN = 5;  
 const byte BLUE  = 6;  
 uint16_t sumOfSamples = 0;  
 uint16_t redData = 0;  
 uint16_t greenData = 0;  
 uint16_t blueData = 0;  
 void setup()   
 {  
  pinMode(S0, OUTPUT);  
  pinMode(S1, OUTPUT);  
  pinMode(S2, OUTPUT);  
  pinMode(S3, OUTPUT);  
  pinMode(Sout, INPUT);  
  // Setting frequency-scaling to 20%  
  digitalWrite(S0,HIGH);  
  digitalWrite(S1,LOW);  
  Serial.begin(250000);  
 }  
 void loop()   
 {  
  int i = 0;  
  // Apply Red Filter  
  digitalWrite(S2,LOW);  
  digitalWrite(S3,LOW);  
  delay(1);  
  sumOfSamples = 0;  
  for (i=0; i < NO_SAMPLES; i++)  
  {  
   // Reading the Pulse Width  
   sumOfSamples += pulseIn(Sout, LOW);  
  }  
  redData = sumOfSamples/NO_SAMPLES;  
  Serial.print("R = ");  
  Serial.print(redData);  
  delay(100);  
  // Apply Green Filter  
  digitalWrite(S2, HIGH);  
  digitalWrite(S3, HIGH);  
  delay(1);  
  sumOfSamples = 0;  
  for (i=0; i < NO_SAMPLES; i++)  
  {  
   // Reading the Pulse Width  
   sumOfSamples += pulseIn(Sout, LOW);  
  }  
  greenData = sumOfSamples/NO_SAMPLES;  
  Serial.print(" G = ");  
  Serial.print(greenData);  
  delay(100);  
  // Apply Blue Filter  
  digitalWrite(S2, LOW);  
  digitalWrite(S3, HIGH);  
  delay(1);  
  sumOfSamples = 0;  
  for (i=0; i < NO_SAMPLES; i++)  
  {  
   // Reading the Pulse Width  
   sumOfSamples += pulseIn(Sout, LOW);  
  }  
  blueData = sumOfSamples/NO_SAMPLES;  
  Serial.print(" B = ");  
  Serial.print(blueData);  
  Serial.println(" ");  
  delay(100);  
  if( isIdle() )  
  {  
   // Turn-Off All Led's  
   Serial.println("All Led's Off");  
   Set_RGB_Color(0,0,0);  
  }  
  if( isRed() )  
  {  
   // Turn-On Red Led  
   Serial.println("Red Led");  
   Set_RGB_Color(255u,0,0); // Red  
  }  
  if( isGreen() )  
  {  
   // Turn-On Green Led  
   Serial.println("Green Led");  
   Set_RGB_Color(0,255u,0); // Green  
  }  
  if( isBlue() )  
  {  
   // Turn-On Blue Led  
   Serial.println("Blue Led");  
   Set_RGB_Color(0,0,255u); // Blue  
  }  
  if( isYellow() )  
  {  
   // Turn-On Yellow Led  
   Serial.println("Yellow Led");  
   Set_RGB_Color(250,75,0); // Yellow  
  }  
  if( isLightGreen() )  
  {  
   // Turn-On Light Green Led  
   Serial.println("Light Green Led");  
   Set_RGB_Color(250,200,50); // Light Green  
  }  
  if( isOrange() )  
  {  
   // Turn-On Orange Led  
   Serial.println("Orange Led");  
   Set_RGB_Color(250,40,0);   // Dark Yellow/ Orange  
  }  
  delay(200);  
 }  
 boolean isIdle( void )  
 {  
  boolean status = false;  
  if ( ((IDLE_RED_THRESHOLD-THRESHOLD) < redData)   
  && (redData < (IDLE_RED_THRESHOLD+THRESHOLD)) )  
  {  
   if ( ((IDLE_GREEN_THRESHOLD-THRESHOLD) < greenData)   
   && (greenData < (IDLE_GREEN_THRESHOLD+THRESHOLD)) )  
   {  
    if ( ((IDLE_BLUE_THRESHOLD-THRESHOLD) < blueData)   
    && (blueData< (IDLE_BLUE_THRESHOLD+THRESHOLD)) )  
    {  
     status = true;  
    }  
   }  
  }  
  return status;  
 }  
 boolean isRed( void )  
 {  
  boolean status = false;  
  if ( ((RED_RED_THRESHOLD-THRESHOLD) < redData)   
  && (redData < (RED_RED_THRESHOLD+THRESHOLD)) )  
  {  
   if ( ((RED_GREEN_THRESHOLD-THRESHOLD) < greenData)   
   && (greenData < (RED_GREEN_THRESHOLD+THRESHOLD)) )  
   {  
    if ( ((RED_BLUE_THRESHOLD-THRESHOLD) < blueData)   
    && (blueData< (RED_BLUE_THRESHOLD+THRESHOLD)) )  
    {  
     status = true;  
    }  
   }  
  }  
  return status;  
 }  
 boolean isGreen( void )  
 {  
  boolean status = false;  
  if ( ((GREEN_RED_THRESHOLD-THRESHOLD) < redData)   
  && (redData < (GREEN_RED_THRESHOLD+THRESHOLD)) )  
  {  
   if ( ((GREEN_GREEN_THRESHOLD-THRESHOLD) < greenData)   
   && (greenData < (GREEN_GREEN_THRESHOLD+THRESHOLD)) )  
   {  
    if ( ((GREEN_BLUE_THRESHOLD-THRESHOLD) < blueData)   
    && (blueData< (GREEN_BLUE_THRESHOLD+THRESHOLD)) )  
    {  
     status = true;  
    }  
   }  
  }  
  return status;  
 }  
 boolean isBlue( void )  
 {  
  boolean status = false;  
  if ( ((BLUE_RED_THRESHOLD-THRESHOLD) < redData)   
  && (redData < (BLUE_RED_THRESHOLD+THRESHOLD)) )  
  {  
   if ( ((BLUE_GREEN_THRESHOLD-THRESHOLD) < greenData)   
   && (greenData < (BLUE_GREEN_THRESHOLD+THRESHOLD)) )  
   {  
    if ( ((BLUE_BLUE_THRESHOLD-THRESHOLD) < blueData)   
    && (blueData< (BLUE_BLUE_THRESHOLD+THRESHOLD)) )  
    {  
     status = true;  
    }  
   }  
  }  
  return status;  
 }  
 boolean isLightGreen( void )  
 {  
  boolean status = false;  
  if ( ((L_GRN_RED_THRESHOLD-THRESHOLD) < redData)   
  && (redData < (L_GRN_RED_THRESHOLD+THRESHOLD)) )  
  {  
   if ( ((L_GRN_GREEN_THRESHOLD-THRESHOLD) < greenData)   
   && (greenData < (L_GRN_GREEN_THRESHOLD+THRESHOLD)) )  
   {  
    if ( ((L_GRN_BLUE_THRESHOLD-THRESHOLD) < blueData)   
    && (blueData< (L_GRN_BLUE_THRESHOLD+THRESHOLD)) )  
    {  
     status = true;  
    }  
   }  
  }  
  return status;  
 }  
 boolean isYellow( void )  
 {  
  boolean status = false;  
  if ( ((YLOW_RED_THRESHOLD-THRESHOLD) < redData)   
  && (redData < (YLOW_RED_THRESHOLD+THRESHOLD)) )  
  {  
   if ( ((YLOW_GREEN_THRESHOLD-THRESHOLD) < greenData)   
   && (greenData < (YLOW_GREEN_THRESHOLD+THRESHOLD)) )  
   {  
    if ( ((YLOW_BLUE_THRESHOLD-THRESHOLD) < blueData)   
    && (blueData< (YLOW_BLUE_THRESHOLD+THRESHOLD)) )  
    {  
     status = true;  
    }  
   }  
  }  
  return status;  
 }  
 boolean isOrange( void )  
 {  
  boolean status = false;  
  if ( ((ORNG_RED_THRESHOLD-THRESHOLD) < redData)   
  && (redData < (ORNG_RED_THRESHOLD+THRESHOLD)) )  
  {  
   if ( ((ORNG_GREEN_THRESHOLD-THRESHOLD) < greenData)   
   && (greenData < (ORNG_GREEN_THRESHOLD+THRESHOLD)) )  
   {  
    if ( ((ORNG_BLUE_THRESHOLD-THRESHOLD) < blueData)   
    && (blueData< (ORNG_BLUE_THRESHOLD+THRESHOLD)) )  
    {  
     status = true;  
    }  
   }  
  }  
  return status;  
 }  
 void Set_RGB_Color( uint8_t red, uint8_t green, uint8_t blue)  
 {  
  analogWrite(RED, red);  
  analogWrite(GREEN, green);  
  analogWrite(BLUE, blue);  
 }  
