const byte S0 4;
const byte S1 5;
const byte S2 6;
const byte S3 7;
const byte SOUT 8;
const byte RED = 3;
const byte GREEN = 10;
const byte BLUE = 11;
const byte BTN = 2;
const byte LPIN = 13;

// Stores frequency read by the photodiodes
int redFrequency = 0;
int greenFrequency = 0;
int blueFrequency = 0;
int lgreenFrequency = 0;
// Stores the red. green and blue colors
int redColor = 0;
int greenColor = 0;
int blueColor = 0;
int lgreenColor = 0;

int buttonState = 0;

void setup()
{
  // Setting the outputs
  pinMode(S0, OUTPUT);
  pinMode(S1, OUTPUT);
  pinMode(S2, OUTPUT);
  pinMode(S3, OUTPUT);

  // Setting the sensorOut as an input
  pinMode(SOUT, INPUT);

  // Setting frequency scaling to 20%
  digitalWrite(S0, HIGH);
  digitalWrite(S1, LOW);

  // Begins serial communication
  Serial.begin(9600);

  pinMode(LPIN, OUTPUT);
  pinMode(BTN, INPUT);
}

void loop()
{
  buttonState = digitalRead(BTN);

  if (buttonState == HIGH)
  {
    digitalWrite(LPIN, HIGH);

    // Setting RED (R) filtered photodiodes to be read
    digitalWrite(S2, LOW);
    digitalWrite(S3, LOW);

    redFrequency = pulseIn(SOUT, LOW);
    redColor = map(redFrequency, 53, 155, 255, 0);

    Serial.print("Red = ");
    Serial.print(redColor);
    delay(100);

    // Setting GREEN (G) filtered photodiodes to be read
    digitalWrite(S2, HIGH);
    digitalWrite(S3, HIGH);

    greenFrequency = pulseIn(SOUT, LOW);
    greenColor = map(greenFrequency, 35, 177, 255, 0);

    Serial.print(" Green = ");
    Serial.print(greenColor);
    delay(100);

    // Setting BLUE (B) filtered photodiodes to be read
    digitalWrite(S2, LOW);
    digitalWrite(S3, HIGH);

    blueFrequency = pulseIn(SOUT, LOW);
    blueColor = map(blueFrequency, 20, 64, 255, 0);

    Serial.print(" Blue = ");
    Serial.print(blueColor);
    delay(100);

    if (redColor > greenColor && redColor > blueColor)
    {
      Serial.println(" - RED detected!");
      Set_RGB_Color(255u, 0, 0);
    }

    if (greenColor > redColor && greenColor > blueColor)
    {
      Serial.println(" - GREEN detected!");
      Set_RGB_Color(0, 255u, 0);
    }

    if (blueColor > redColor && blueColor > greenColor)
    {
      Serial.println(" - BLUE detected!");
      Set_RGB_Color(0, 0, 255u);
    }
  }
  else
  {
    // turn LED off:
    digitalWrite(LPIN, LOW);
  }
}

void Set_RGB_Color(uint8_t red, uint8_t green, uint8_t blue)
{
  analogWrite(RED, red);
  analogWrite(GREEN, green);
  analogWrite(BLUE, blue);
}